colorscheme default
set background=light
syntax on                  " Enable syntax highlighting.
set nocompatible
set noshowmode
set autoshelldir  
set t_Co=256
set novisualbell
set encoding=UTF-8
set laststatus=2
set showtabline=2
set updatetime=50
set autoindent             " Respect indentation when starting a new line.
set expandtab              " Expand tabs to spaces. Essential in Python.
set tabstop=4              " Number of spaces tab is counted for.
set shiftwidth=4           " Number of spaces to use for autoindent.
set backspace=2            " Fix backspace behavior on most terminals.
set nowrap
packloadall                     " Load all plugins.
silent! helptags ALL            " Load help files for all plugins.
set wildmenu                    " Enable enhanced tab autocomplete.
set wildmode=full  " Complete till longest string, then open menu.
set number                      " Display column numbers.
set relativenumber              " Display relative column numbers.
set nohlsearch                    " Highlight search results.
set undofile
set incsearch                   " Search as you type.
set guifont=JetBrainsMono\ Nerd\ Font\ Mono:h10:cANSI "set font for gui version of vim/nvim
set is
set si
" set guioptions-=m  "menu bar
" set guioptions-=T  "toolbar
" set guioptions-=r  "scrollbar
set go-=e

"8.2 Always change the directory to working directory of file in current buffer - http://vim.wikia.com/wiki/VimTip64
function! CHANGE_CURR_DIR()
    let _dir = expand("%:p:h")
    exec "cd " . _dir
    unlet _dir
endfunction

" toggle button to show gui buttons and topbar
" function! ToggleGUICruft()
"   if &guioptions=='i'
"     exec('set guioptions=imTrL')
"   else
"     exec('set guioptions=i')
"   endif
" endfunction

" nnoremap <leader>gui <Esc>:call ToggleGUICruft()<CR>

" by default, hide gui menus
" set guioptions=i

" -------------------------------------------------------
"some keybinds for convenience
let mapleader = " "
nmap tw :w!<cr>
nmap twq :wq!<cr>
nmap tws :wq! && :so%<cr>
nmap tq :q!<cr>
nmap ts :so %<cr>

"some windows keybinds
nmap ss :split<Return><C-w>w
nmap sv :vsplit<Return><C-w>w
nmap tn :tabnew<Return><C-w>w

"tab movement
nmap <tab> :tabnext<Return>
nmap <S-tab> :tabprevious<Return>
" nmap <leader> <C-w>w
map s<left> <C-w>h
map s<up> <C-w>k
map s<down> <C-w>j
map s<right> <C-w>l
map sh <C-w>h
map sl <C-w>l
map sj <C-w>j
map sk <C-w>k

"for resizing windows 
nmap <C-w><left> <C-w><
nmap <C-w><right> <C-w>>
nmap <C-w><up> <C-w>+
nmap <C-w><down> <C-w>-

"fzf keymaps
nnoremap <leader>fr :History<CR>
nnoremap <leader>fzf :FZF ~<CR>
nnoremap sf :Ex<CR>
nnoremap <leader>fi :FZF C:\Users\sunny\Downloads\dotfiles\LegioN2004-githubthings\programs<CR>
nnoremap <leader>dot :FZF C:\Users\sunny\Downloads\dotfiles\LegioN2004-githubthings\dotfiles<CR>

" maximizer
nnoremap <leader>sm :MaximizerToggle<CR>

" undotree
nnoremap <leader>un :UndotreeToggle<CR>

" going up and down with cursor in the center of the buffer
nnoremap <C-u> <C-u>zz
nnoremap <C-d> <C-d>zz
nnoremap n nzzzv
nnoremap N Nzzzv

" netrw thing
nnoremap <leader>e <Esc>:Lex<CR>:vertical resize 30<CR>
let g:netrw_liststyle = 3

" pasting from clipboard instead of the vim register fix this afterwards
nnoremap <leader>Y "+y 
"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"just to make those backup files in another directory so as to not make a mess
set undofile
let &undodir = expand('~\vimfiles\undo')

if !isdirectory(&undodir) | call mkdir(&undodir, "p") | endif


autocmd filetype cpp nnoremap <leader>f :w <bar> !g++ -std=c++14 % -o %:r -Wl,--stack,268435456<CR>
autocmd filetype cpp nnoremap <leader>r :!%:r<CR>
autocmd filetype cpp nnoremap <C-C> :s/^\(\s*\)/\1\/\/<CR> :s/^\(\s*\)\/\/\/\//\1<CR> $
